﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Globalization;
using Moq;
using System.Text.RegularExpressions;

namespace AKQA.Tests
{
    [TestFixture]
    public class BatchProcessorTests
    {
        [Test]
        [TestCase("0900 1730", 9, 0, 17, 30)]
        [TestCase("1000 1830", 10, 0, 18, 30)]
        [TestCase("1800 1730", 9, 0, 17, 30, ExpectedException = typeof(ArgumentOutOfRangeException))]
        [TestCase("foobar", 9, 0, 17, 30, ExpectedException = typeof(FormatException))]
        [TestCase(" ", 9, 0, 17, 30, ExpectedException = typeof(ArgumentNullException))]
        [TestCase("foobar12", 9, 0, 17, 30, ExpectedException = typeof(FormatException))]
        [TestCase("0960 1234", 9, 0, 17, 30, ExpectedException = typeof(OverflowException))]
        public void GivenAnInstance_WhenProcessingHoursOnly_ThenValuesAreValidated(string input, int startHour, int startMinute, int endHour, int endMinute)
        {
            //Arrange
            var mockMeetingScheduler = CreateMockScheduler();

            var sut = new BatchProcessor();
            sut.MeetingScheduler = mockMeetingScheduler.Object;

            //Act
            var result = sut.ProcessMultiple(new[] { input });

            //Assert
            Assert.IsNotNull(result);
            mockMeetingScheduler.VerifySet(x => x.StartTime);
            mockMeetingScheduler.VerifySet(x => x.EndTime);
        }

        private static Mock<IMeetingScheduler> CreateMockScheduler()
        {
            var mockMeetingScheduler = new Mock<IMeetingScheduler>();
            mockMeetingScheduler.SetupAllProperties();
            mockMeetingScheduler
                .Setup(x => x.GeneratePlan())
                .Returns(new List<MeetingRequest>());
            return mockMeetingScheduler;
        }

        [Test]
        public void GivenAnInstance_ProcessingASingleRequest_ThenItCallsDependenciesAccurately()
        {
            // Arrange
            var input = @"0900 1730
2011-03-17 10:17:06 EMP001
2011-03-21 09:00 2";
            MeetingRequest expected = new MeetingRequest
            {
                EmployeeId = "EMP001",
                RequestDate = new DateTime(2011, 3, 17, 10, 17, 6),
                MeetingStart = new DateTime(2011, 3, 21, 9, 0, 0),
                MeetingEnd = new DateTime(2011, 3, 21, 11, 0, 0)
            };
            var mockMeetingScheduler = CreateMockScheduler();
            MeetingRequest output = null;
            mockMeetingScheduler
                .Setup(x => x.AddRequest(It.IsAny<MeetingRequest>()))
                .Callback<MeetingRequest>(x => output = x);

            var sut = new BatchProcessor();
            sut.MeetingScheduler = mockMeetingScheduler.Object;

            // Act
            var result = sut.ProcessMultiple(Regex.Split(input, "\r\n"));

            // Assert
            mockMeetingScheduler.Verify(x => x.AddRequest(It.IsAny<MeetingRequest>()));
            Assert.AreEqual(expected, output);
        }

        [Test]
        public void GivenAnInstance_WhenMockDataIsPassed_ThenResultIsFormattedCorrectly()
        {
            // Arrange

            // This is not really used, any other valid input with at least one record would do
            var input = @"0900 1730
2011-03-17 10:17:06 EMP001
2011-03-21 09:00 2";

            var expected = @"2011-03-21
09:00 11:00 EMP001
12:00 13:00 EMP002
2011-03-22
09:00 11:00 EMP003
";

            var mockData = new List<MeetingRequest>
            {
                new MeetingRequest
                {
                    EmployeeId = "EMP001",
                    RequestDate = new DateTime(2011, 3, 17, 10, 17, 6),
                    MeetingStart = new DateTime(2011, 3, 21, 9, 0, 0),
                    MeetingEnd = new DateTime(2011, 3, 21, 11, 0, 0)
                },
            
                new MeetingRequest
                {
                    EmployeeId = "EMP002",
                    RequestDate = new DateTime(2011, 3, 17, 10, 17, 6),
                    MeetingStart = new DateTime(2011, 3, 21, 12, 0, 0),
                    MeetingEnd = new DateTime(2011, 3, 21, 13, 0, 0)
                },

                new MeetingRequest
                {
                    EmployeeId = "EMP003",
                    RequestDate = new DateTime(2011, 3, 17, 10, 17, 6),
                    MeetingStart = new DateTime(2011, 3, 22, 9, 0, 0),
                    MeetingEnd = new DateTime(2011, 3, 22, 11, 0, 0)
                }
            };

            var mockMeetingScheduler = CreateMockScheduler();

            mockMeetingScheduler
                .Setup(x => x.GeneratePlan())
                .Returns(mockData);
            var sut = new BatchProcessor();
            sut.MeetingScheduler = mockMeetingScheduler.Object;

            // Act
            var result = sut.ProcessMultiple(Regex.Split(input, "\r\n"));

            // Assert
            mockMeetingScheduler.Verify(x => x.GeneratePlan());
            Assert.AreEqual(expected, result);
        }
    }
}
