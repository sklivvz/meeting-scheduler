﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace AKQA.Tests
{
    [TestFixture]
    public class MeetingSchedulerTests
    {
        private static MeetingScheduler CreateMeetingScheduler()
        {
            var sut = new MeetingScheduler();
            sut.StartTime = new TimeSpan(9, 0, 0);
            sut.EndTime = new TimeSpan(17, 30, 00);
            return sut;
        }
        private static MeetingRequest CreateMeetingRequest()
        {
            var req = new MeetingRequest
            {
                RequestDate = new DateTime(2011, 3, 17, 10, 17, 6),
                MeetingStart = new DateTime(2011, 3, 21, 9, 0, 0),
                MeetingEnd = new DateTime(2011, 3, 21, 11, 0, 0),
                EmployeeId = "EMP001"
            };
            return req;
        }

        [Test]
        [TestCase(9, 0)]
        [TestCase(1, 0)]
        [TestCase(10, 22)]
        [TestCase(25, 22), ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GivenAnEmptyInstance_AStartTime_CanBeSet(int hours, int minutes)
        {
            //Arrange
            var sut = new MeetingScheduler();

            //Act
            sut.StartTime = new TimeSpan(9, 0, 0);

            //Assert
            Assert.Pass();
        }

        [Test]
        [TestCase(9, 0)]
        [TestCase(1, 0)]
        [TestCase(10, 22)]
        [TestCase(25, 22), ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GivenAnEmptyInstance_AnEndTime_CanBeSet(int hours, int minutes)
        {
            //Arrange
            var sut = new MeetingScheduler();

            //Act
            sut.EndTime = new TimeSpan(9, 0, 0);

            //Assert
            Assert.Pass();
        }

        [Test]
        public void GivenAnEmptyInstance_ASingleValidRequest_CanBeAdded()
        {
            //Arrange
            var sut = CreateMeetingScheduler();
            var req = CreateMeetingRequest();

            //Act
            sut.AddRequest(req);

            //Assert
            Assert.Pass();
        }

        [Test]
        public void GivenAnEmptyInstance_ASingleValidRequest_IsReturned()
        {
            //Arrange
            var sut = CreateMeetingScheduler();
            var req = CreateMeetingRequest();
            sut.AddRequest(req);

            //Act
            var result = sut.GeneratePlan();

            //Assert
            Assert.AreEqual(req, result.Single());
        }

        [Test]
        [TestCase(8, 12)]
        [TestCase(16, 18)]
        [TestCase(8, 18)]
        [Description("No part of a meeting may fall outside office hours.")]
        public void GivenAnEmptyInstance_ASingleInvalidRequest_IsNotReturned(int startHour, int endHour)
        {
            //Arrange
            var sut = CreateMeetingScheduler();
            var req = CreateMeetingRequest();
            req.MeetingStart = new DateTime(2011, 3, 21, startHour, 0, 0);
            req.MeetingEnd = new DateTime(2011, 3, 21, endHour, 0, 0);

            sut.AddRequest(req);

            //Act
            var result = sut.GeneratePlan();

            //Assert
            Assert.IsTrue(!result.Any());
        }

        [Test]
        [TestCase(0, 24, true)]
        [TestCase(0, 23, false)]
        [TestCase(1, 24, false)]
        public void GivenAnEmptyInstance_CrossDayRequests_AreHandled(int dayStart, int dayEnd, bool expected)
        {
            //Arrange
            var sut = CreateMeetingScheduler();
            sut.StartTime = new TimeSpan(dayStart, 0, 0);
            sut.EndTime = new TimeSpan(dayEnd, 0, 0);
            var req = CreateMeetingRequest();
            req.MeetingStart = new DateTime(2011, 3, 20, 9, 0, 0);
            req.MeetingEnd = new DateTime(2011, 3, 21, 11, 0, 0);
            sut.AddRequest(req);
            
            //Act
            var result = sut.GeneratePlan();

            //Assert
            if (expected)
                Assert.AreEqual(req, result.Single());
            else
                Assert.IsTrue(!result.Any());
        }

        [Test]
        public void GivenAnEmptyInstance_WhenSeparateRequestsAreAdded_BothAreReturned()
        {
            //Arrange
            var sut = CreateMeetingScheduler();
            var req1 = CreateMeetingRequest();
            var req2 = CreateMeetingRequest();
            req2.RequestDate = req1.RequestDate.AddSeconds(1);
            req2.MeetingStart = req1.MeetingStart.AddDays(1);
            req2.MeetingEnd = req1.MeetingEnd.AddDays(1);
            sut.AddRequest(req1);
            sut.AddRequest(req2);

            //Act
            var result = sut.GeneratePlan().ToList();

            //Assert
            Assert.Contains(req1,result);
            Assert.Contains(req2,result);
            Assert.AreEqual(2, result.Count);
        }

        [Test]
        [Description("Meetings may not overlap.")]
        public void GivenAnEmptyInstance_WhenTwoOverlapRequestsAreAdded_OnlyOneIsReturned()
        {
            //Arrange
            var sut = CreateMeetingScheduler();
            var req1 = CreateMeetingRequest();
            var req2 = CreateMeetingRequest();
            req2.RequestDate = req1.RequestDate.AddSeconds(1);
            req2.MeetingStart = req1.MeetingStart.AddHours(1);
            req1.MeetingEnd = req2.MeetingEnd.AddHours(1);
            req2.MeetingEnd = req1.MeetingEnd.AddHours(1);
            sut.AddRequest(req1);
            sut.AddRequest(req2);

            //Act
            var result = sut.GeneratePlan().ToList();

            //Assert
            Assert.AreEqual(1, result.Count);
        }

        [Test]
        public void GivenAnInstanceFilledAsInTheExample_WhenThePlanIsGenerated_ThenTheExpectedOutputIsReturned()
        {
            //Arrange
            var sut = CreateMeetingScheduler();
            /*0900 1730*/
            sut.StartTime = new TimeSpan(9, 0, 0);
            sut.EndTime = new TimeSpan(17, 30, 0);

            /*2011-03-17 10:17:06 EMP001
            2011-03-21 09:00 2*/
            sut.AddRequest(new MeetingRequest
            {
                 RequestDate = new DateTime(2011,3,17,10,17,6),
                 EmployeeId = "EMP001",
                 MeetingStart = new DateTime(2011, 3, 21, 9, 0, 0),
                 MeetingEnd = new DateTime(2011, 3, 21, 11, 0, 0)
            });

            /*2011-03-16 12:34:56 EMP002
            2011-03-21 09:00 2*/
            sut.AddRequest(new MeetingRequest
            {
                RequestDate = new DateTime(2011, 3, 16, 12, 34, 56),
                EmployeeId = "EMP002",
                MeetingStart = new DateTime(2011, 3, 21, 9, 0, 0),
                MeetingEnd = new DateTime(2011, 3, 21, 11, 0, 0)
            });

            /*2011-03-16 09:28:23 EMP003
            2011-03-22 14:00 2*/
            sut.AddRequest(new MeetingRequest
            {
                RequestDate = new DateTime(2011, 3, 16, 9, 28, 23),
                EmployeeId = "EMP003",
                MeetingStart = new DateTime(2011, 3, 22, 14, 0, 0),
                MeetingEnd = new DateTime(2011, 3, 22, 16, 0, 0)
            });

            /*2011-03-17 10:17:06 EMP004
            2011-03-22 16:00 1*/
            //TODO: Not clear what to do here as this breaks this given condition:
            //"The booking submission system only allows one submission at a time, so submission times are guaranteed to be unique."
            //I will be changing the timestamp to something
            //different as to produce the expected result
            sut.AddRequest(new MeetingRequest
            {
                RequestDate = new DateTime(2011, 3, 17, 10, 17, 5),
                EmployeeId = "EMP004",
                MeetingStart = new DateTime(2011, 3, 22, 16, 0, 0),
                MeetingEnd = new DateTime(2011, 3, 22, 17, 0, 0)
            });

            /*2011-03-15 17:29:12 EMP005
            2011-03-21 16:00 3*/
            sut.AddRequest(new MeetingRequest
            {
                RequestDate = new DateTime(2011, 3, 15, 17, 29, 12),
                EmployeeId = "EMP005",
                MeetingStart = new DateTime(2011, 3, 21, 16, 0, 0),
                MeetingEnd = new DateTime(2011, 3, 21, 19, 0, 0)
            });
            //Act
            var result = sut.GeneratePlan().ToList();

            //Assert
            Assert.AreEqual(3, result.Count);
            /*2011-03-21
            09:00 11:00 EMP002*/
            Assert.AreEqual("EMP002", result[0].EmployeeId);
            Assert.AreEqual(new DateTime(2011, 3, 21, 9, 0, 0), result[0].MeetingStart);
            Assert.AreEqual(new DateTime(2011, 3, 21, 11, 0, 0), result[0].MeetingEnd);
            /*2011-03-22
            14:00 16:00 EMP003
            16:00 17:00 EMP004*/
            Assert.AreEqual("EMP003", result[1].EmployeeId);
            Assert.AreEqual(new DateTime(2011, 3, 22, 14, 0, 0), result[1].MeetingStart);
            Assert.AreEqual(new DateTime(2011, 3, 22, 16, 0, 0), result[1].MeetingEnd);
            Assert.AreEqual("EMP004", result[2].EmployeeId);
            Assert.AreEqual(new DateTime(2011, 3, 22, 16, 0, 0), result[2].MeetingStart);
            Assert.AreEqual(new DateTime(2011, 3, 22, 17, 0, 0), result[2].MeetingEnd);
        }
    }
}
