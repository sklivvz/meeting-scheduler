﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Autofac;

namespace AKQA
{
    /// <summary>
    /// Very thin layer to present the application as a console app.
    /// Note: there are no unit tests for this.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            string output;

            using (var container = ConfigureInjection())
            {
                var processor = container.Resolve<IBatchProcessor>();
                output = processor.ProcessMultiple(Console.In.AsEnumerable());
            }

            Console.Write(output);
        }

        /// <summary>
        /// Autofac configuration.
        /// </summary>
        /// <returns>The (single) IoC container for use.</returns>
        static IContainer ConfigureInjection()
        {
            var builder = new ContainerBuilder();
            builder.Register<IBatchProcessor>(x => new BatchProcessor()).PropertiesAutowired();
            builder.Register<IMeetingScheduler>(x => new MeetingScheduler()).PropertiesAutowired();
            return builder.Build();
        }
    }

}
