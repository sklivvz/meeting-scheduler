﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AKQA
{
    /// <summary>
    /// This class takes care of taking a list of meeting requests and calculating a resulting schedule.
    /// </summary>
    public class MeetingScheduler : IMeetingScheduler
    {
        SortedList<DateTime, MeetingRequest> _requests = new SortedList<DateTime, MeetingRequest>();

        /// <summary>
        /// Adds a new request
        /// </summary>
        /// <param name="request">A meeting request to be added</param>
        /// <exception cref="System.ArgumentException">You tried to add two requests with the same request timestamp. It should be unique.</exception>
        public void AddRequest(MeetingRequest request)
        {
            //Meeting across days: they are OK if we are open 24/7
            //TODO: check for weekends :-)
            if (IsOpenAllDay)
            {
                _requests.Add(request.RequestDate, request);
                return;
            }

            if ((request.MeetingEnd - request.MeetingStart).TotalHours > 24)
                return;

            //Not across days, and not open 24/7: meetings only inside office hours
            if (request.MeetingStart - request.MeetingStart.Date < StartTime ||
                request.MeetingEnd - request.MeetingEnd.Date > EndTime)
            {
                return;
            }

            _requests.Add(request.RequestDate, request);
        }

        /// <summary>
        /// Generates a list of valid meetings sorted by meeting start date.
        /// </summary>
        /// <returns>The meetings as a <c ref="IEnumerable<MeetingRequest>" /></returns>
        public IEnumerable<MeetingRequest> GeneratePlan()
        {
            // result must be sorted by meeting start
            var result = new SortedList<DateTime, MeetingRequest>();

            // Values are sorted by request date. 
            // Older requests prevent newer requests from applying if overlapping
            foreach (var request in _requests.Values)
            {
                // are there any older requests overlapping? 
                var overlaps = result.Values.Where(x => x.OverlapsWith(request)).Any();
                if (!overlaps)
                    result.Add(request.MeetingStart, request);
            }

            // Can't yield because of the sorting. 
            // Even if sorting was in the presentation class, it would still need to enumerate.
            return result.Values;
        }

        /// <summary>
        /// The office work day start time
        /// </summary>
        public TimeSpan StartTime
        {
            get;
            set;
        }

        /// <summary>
        /// The office work day end time
        /// </summary>
        public TimeSpan EndTime
        {
            get;
            set;
        }

        /// <summary>
        /// Returns <cref="true"> if the company is open 24H/24H.
        /// </summary>
        private bool IsOpenAllDay
        {
            get
            {
                return StartTime == new TimeSpan(0, 0, 0) && EndTime == new TimeSpan(24, 0, 0);
            }
        }
    }
}
