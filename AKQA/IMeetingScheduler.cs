﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AKQA
{
    /// <summary>
    /// Represents a class capable of scheduling a series of meeting requests
    /// </summary>
    public interface IMeetingScheduler
    {
        /// <summary>
        /// The start of the work day
        /// </summary>
        TimeSpan StartTime { get; set; }

        /// <summary>
        /// The end of the work day
        /// </summary>
        TimeSpan EndTime { get; set; }

        /// <summary>
        /// Adds a request to the batch
        /// </summary>
        /// <param name="request">A MeetingRequest which represents a request to be eventually scheduled</param>
        void AddRequest(MeetingRequest request);

        /// <summary>
        /// Calculates the meeting schedule based on the requests accumulated.
        /// </summary>
        /// <returns>A meeting schedule as a IEnumerable collection of Meeting requests, sorted by meeting start date time</returns>
        IEnumerable<MeetingRequest> GeneratePlan();
    }
}
