﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace AKQA
{
    /// <summary>
    /// This class processes textual input in the format specified in the brief and formats a series of meeting requests
    /// as per the expected output format.
    /// This class does not do any scheduling, only "presentation"
    /// </summary>
    public class BatchProcessor : IBatchProcessor
    {
        /// <summary>
        /// Takes a list of meeting requests as a long string and
        /// returns a list of meetings grouped by day, without overlaps
        /// </summary>
        /// <param name="input">An input string as per "Meeting Scheduler Tests" requirements</param>
        /// <returns>An output string as per "Meeting Scheduler Tests" requirements</returns>
        public string ProcessMultiple(IEnumerable<string> input)
        {
            var inLines = input.ToArray();

            if (inLines.Length == 0)
                throw new ArgumentNullException("input");

            ProcessHeader(inLines[0]);

            if (inLines.Length > 1)
            {
                for (int i = 1; i < inLines.Length; i += 2)
                {
                    if (i + 1 >= inLines.Length)
                        throw new FormatException("Abnormal input termination");

                    ProcessRecord(inLines[i], inLines[i + 1]);
                }
            }

            var currentDay = DateTime.MinValue;
            var builder = new StringBuilder();

            var result = MeetingScheduler.GeneratePlan();

            foreach (var meeting in result)
            {
                if (meeting.MeetingStart.Date != currentDay)
                {
                    currentDay = meeting.MeetingStart.Date;
                    builder.AppendFormat("{0:yyyy-MM-dd}\r\n", currentDay);
                }
                builder.AppendFormat("{0:HH:mm} {1:HH:mm} {2}\r\n", meeting.MeetingStart, meeting.MeetingEnd, meeting.EmployeeId);
            }

            return builder.ToString();
        }

        /// <summary>
        /// Processes the header line, expects something in the format "HHMM HHMM" with the first hour coming before the second
        /// </summary>
        /// <param name="header">The first line of the input</param>
        private void ProcessHeader(string header)
        {
            if (string.IsNullOrWhiteSpace(header))
                throw new ArgumentNullException("input");
            if (header.Length != 9)
                throw new FormatException("input");

            var parts = new[] { header.Substring(0, 4), header.Substring(5) };

            MeetingScheduler.StartTime = TimeSpan.ParseExact(parts[0], "hhmm", CultureInfo.InvariantCulture);
            MeetingScheduler.EndTime = TimeSpan.ParseExact(parts[1], "hhmm", CultureInfo.InvariantCulture);

            if (MeetingScheduler.StartTime > MeetingScheduler.EndTime)
                throw new ArgumentOutOfRangeException("input");
        }

        /// <summary>
        /// Processes a record block of the input (2 lines, representing one request).
        /// </summary>
        /// <param name="firstLine">The first line of the record block</param>
        /// <param name="secondLine">The second line of the record block</param>
        /// <remarks>Please see the brief for documentation of the format.</remarks>
        private void ProcessRecord(string firstLine, string secondLine)
        {
            if (string.IsNullOrWhiteSpace(firstLine) || firstLine.Length < 21)
                throw new FormatException("Invalid record");

            if (string.IsNullOrWhiteSpace(secondLine) || secondLine.Length < 18)
                throw new FormatException("Invalid record");

            var request = new MeetingRequest();

            var requestDateStr = firstLine.Substring(0, 19);
            request.RequestDate = DateTime.Parse(requestDateStr);

            request.EmployeeId = firstLine.Substring(20);

            var meetingDateStr = secondLine.Substring(0, 16);
            request.MeetingStart = DateTime.Parse(meetingDateStr);

            var meetingLengthStr = secondLine.Substring(17);
            request.MeetingEnd = request.MeetingStart.AddHours(int.Parse(meetingLengthStr));

            MeetingScheduler.AddRequest(request);
        }

        // Dependency property
        public IMeetingScheduler MeetingScheduler { get; set; }
    }
}
