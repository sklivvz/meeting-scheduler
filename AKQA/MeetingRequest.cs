﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AKQA
{
    /// <summary>
    /// A data object representing a meeting request
    /// </summary>
    public class MeetingRequest: IEquatable<MeetingRequest>
    {
        /// <summary>
        /// The unique timestamp of the request
        /// </summary>
        public DateTime RequestDate { get; set; }

        /// <summary>
        /// The meeting start time
        /// </summary>
        public DateTime MeetingStart { get; set; }

        /// <summary>
        /// The meeting end time
        /// </summary>
        public DateTime MeetingEnd { get; set; }

        /// <summary>
        /// The Id of the employee who made the request
        /// </summary>
        public string EmployeeId { get; set; }

        /// <summary>
        /// Determines if this meeting overlaps with another.
        /// </summary>
        /// <param name="other">The other meeting.</param>
        /// <returns>Returns <c ref="true"/> if this meeting overlaps with the other, <c ref="false"/> otherwise.</returns>
        internal bool OverlapsWith(MeetingRequest other)
        {
            return other.Contains(MeetingStart) || other.Contains(MeetingEnd);
        }
        
        /// <summary>
        /// Determines if a given date time is contained within the boundaries of this meeting
        /// </summary>
        /// <param name="value">The given value</param>
        /// <returns></returns>
        private bool Contains(DateTime value)
        {
            return value > MeetingStart && value <= MeetingEnd;
        }

        /// <summary>
        /// Determines if this instance is equal to another
        /// </summary>
        /// <param name="other">The other instance</param>
        /// <returns>Returns <c ref="true"/> if this meeting is equal to the other, <c ref="false"/> otherwise.</returns>
        public bool Equals(MeetingRequest other)
        {
            if (other == null) return false;
            return 
                RequestDate == other.RequestDate && 
                MeetingStart == other.MeetingStart && 
                MeetingEnd == other.MeetingEnd && 
                EmployeeId == other.EmployeeId;
        }
    }
}
