﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AKQA
{
    /// <summary>
    /// Represents a class capable of processing a batch of textual lines and returning an output
    /// </summary>
    public interface IBatchProcessor
    {
        /// <summary>
        /// Processes multiple text lines and returns a formatted output
        /// </summary>
        /// <param name="input">An IEnumerable collection of text lines</param>
        /// <returns>The output, as a string.</returns>
        string ProcessMultiple(IEnumerable<string> input);
    }
}
