﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

//TODO: test this

namespace AKQA
{
    /// <summary>
    /// Small utility class to simplify the code in Program. Extends TextReader
    /// </summary>
    public static class TextReaderExtensions
    {
        /// <summary>
        /// Extension method to pipe a text stream to an <c ref="IEnumerable<string>"/>
        /// </summary>
        public static IEnumerable<string> AsEnumerable(this TextReader stream)
        {
            string line;
            while ((line = Console.ReadLine()) != null) yield return line;
        }
    }
}
